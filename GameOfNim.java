import java.util.Random;
import java.util.Scanner;
/**
 * GameOfNim plays a game between a human and computer generated user.
 * 
 * @author Samantha Tran
 * @version April 2017
 */
public class GameOfNim
{
    private int turn, marbles, gameDifficulty, remove;

    /**
     * Constructor for objects of class GameOfNim
     * @param min the minimum amount of marbles
     * @param max the maximum amount of marbles
     */
    public GameOfNim(int minAmountOfMarbles, int maxAmountOfMarbles)
    {
      Random generator = new Random();
      marbles = generator.nextInt(maxAmountOfMarbles - minAmountOfMarbles) + minAmountOfMarbles + 1;
      turn = generator.nextInt(2);
      gameDifficulty = generator.nextInt(2);
    }

    /**
     * This Play method runs the game of nim. (There is no return type - void)
     */
    public void play()
    {
       System.out.println("Initially there are " + marbles + " marbles in the pile"); 
       
       while (marbles > 1) {

           if (turn == 0) {
               if (gameDifficulty == 0)
               {
                   Random computer = new Random();
                   remove = computer.nextInt(marbles / 2) + 1;
                   System.out.print("Computer's turn:   Removed " + remove + " marble(s).");
                   marbles -= remove;
                   System.out.print("\tCurrent number of marbles: " + marbles + "\n");
               } else
               {
                   int inc = 0;
                   double power;
                   do {
                       inc++;
                       power = Math.pow(2, inc);
                   }
                   while (power < marbles);
                   inc--;
                   power = Math.pow(2, inc);
                   remove = (marbles - ((int) power - 1));

                   System.out.print("Computer's turn:   Removed " + remove + " marble(s).");
                   marbles -= remove;
                   System.out.print("\tCurrent number of marbles: " + marbles + "\n");
               }
               turn = 1;
           } else {

               Scanner number = new Scanner(System.in);
               System.out.print("Human's turn: ");
               remove = number.nextInt();
               if (remove >= 1 && remove <= marbles / 2)
               {
                   System.out.print("Removed " + remove + " marble(s).");
                   marbles -= remove;
                   System.out.print("\tCurrent number of marbles: " + marbles + "\n");
               } else {
                   System.out.print("Sorry, your number is not within bounds, try again: ");
                   remove = number.nextInt();
                   System.out.print("Removed " + remove + " marble(s).");
                   marbles -= remove;
                   System.out.print("\tCurrent number of marbles: " + marbles + "\n");
               }
               turn = 0;

           }

       }
       
       if (marbles == 1)
       {
           if (turn == 0)
             System.out.print("Human wins!!");
           else
             System.out.print("Computer wins!!");
       }
       else if (marbles == 0)
       {
           if (turn == 1)
             System.out.print("Human wins!!");
           else
             System.out.print("Computer wins!!");
        }

    }
}
